# plain-gib - barebones html/typescript front end

currently, I'm using this as a minimal front end for testing consuming ibgib
libraries.

## testing

### importmaps - `npm run test:browser` fails

as a workaround for bleeding edge ES module consumption (let alone testing
frameworks for them), I have kluged a workaround by editing
`/node_modules/jasmine-browser-runner/run.html.ejs` to include an import map
section. So atow paste the following code before any other `script` tags in the
`head` section.  (the versions may need to change):

_note: atow I am copy/pasting this text whenever I reinstall node_modules folder_

```html
  <script type="importmap">
    {
      "imports": {
        "ts-gib": "https://unpkg.com/ts-gib@0.4.8/dist/index.mjs",
        "ts-gib/": "https://unpkg.com/ts-gib@0.4.8/"
      }
    }
  </script>
```
